﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogFileEntityModel.Enum;
using LogFileEntityModel.PropertiesModels;

namespace LogFileEntityModel
{
    public class BaseRenderModel
    {
        public DateModel Date { get; set; }
        public string Process { get; set; }
        public LogMode LogMode { get; set; }
        public string Provider { get; set; }
        public string Message { get; set; }

        public string LogMessage { get; set; }

        public BaseRenderModel()
        {
            
        }

        public BaseRenderModel(BaseRenderModel baseRenderModel)
        {
            Date = baseRenderModel.Date;
            Process = baseRenderModel.Process;
            LogMode = baseRenderModel.LogMode;
            Provider = baseRenderModel.Provider;
            Message = baseRenderModel.Message;
            LogMessage = baseRenderModel.LogMessage;
        }
    }
}
