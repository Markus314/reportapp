﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogFileEntityModel.Enum
{
    public enum LogMode
    {
        Debug,
        Info,
        Warn,
        Error
    }
}
