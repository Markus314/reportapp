﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogFileEntityModel.PropertiesModels;

namespace LogFileEntityModel
{
    public class GetRenderingModel : BaseRenderModel
    {
        public UidModel Uid { get; set; }

        public GetRenderingModel()
        {
            
        }

        public GetRenderingModel(GetRenderingModel getRendering)
        {
            Uid = getRendering.Uid;
        }

        public GetRenderingModel(UidModel uidModel)
        {
            Uid = uidModel;
        }
    }
}
