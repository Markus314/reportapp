﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace LogFileEntityModel.PropertiesModels
{
    public class DateModel : IXmlSerializable
    {
        public DateTime Date { get; set; }
        public string SSS { get; set; }

        public DateModel()
        {
            
        }

        public DateModel(DateModel dateModel)
        {
            Date = dateModel.Date;
            SSS = dateModel.SSS;
        }

        public override string ToString()
        {
            return $"{Date.ToString(@"yyyy-mm-dd hh:mm:ss",CultureInfo.InvariantCulture)},{SSS}";
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            var dateStr = reader.ReadString();
            var splittedDate = dateStr.Split(',');

            Date = DateTime.Parse(splittedDate[0]);
            SSS = splittedDate[1];
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteString(ToString());
        }
    }
}
