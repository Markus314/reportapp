﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace LogFileEntityModel.PropertiesModels
{
    public class UidModel : IXmlSerializable, IEquatable<UidModel>
    {
        public long IdRightPart { get; set; }
        public long IdLeftPart { get; set; }

        public UidModel()
        {
            
        }

        public UidModel(UidModel uid)
        {
            IdRightPart = uid.IdRightPart;
            IdLeftPart = uid.IdLeftPart;
        }

        #region Overriden methods

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public bool Equals(UidModel other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return IdRightPart == other.IdRightPart && IdLeftPart == other.IdLeftPart;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (IdRightPart.GetHashCode() * 397) ^ IdLeftPart.GetHashCode();
            }
        }

        public static bool operator ==(UidModel left, UidModel right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(UidModel left, UidModel right)
        {
            return !Equals(left, right);
        }


        public override string ToString()
        {
            return $"{IdLeftPart}-{IdRightPart}";
        }
        #endregion

        #region IXmlSerialization methods

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            var dateStr = reader.ReadString();
            var splittedDate = dateStr.Split('-');

            IdLeftPart = long.Parse(splittedDate[0]);
            IdRightPart = long.Parse(splittedDate[1]);
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteString(ToString());
        }

        #endregion
    }
}
