﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogFileEntityModel.Enum;
using LogFileEntityModel.PropertiesModels;

namespace LogFileEntityModel
{
    public class RenderingReturnedModel : BaseRenderModel
    {
        public UidModel Uid { get; set; }

        #region Constructors

        public RenderingReturnedModel()
        {
            
        }

        public RenderingReturnedModel(RenderingReturnedModel RenderReturned)
        {
            Uid = RenderReturned.Uid;
        }

        public RenderingReturnedModel(UidModel uidModel)
        {
            Uid = uidModel;
        }
        #endregion
    }
}
