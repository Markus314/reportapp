﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogFileEntityModel.Enum;

namespace LogFileEntityModel
{
    public class StartRenderingModel : BaseRenderModel
    {
        public int? FirstArgument { get; set; }
        public int? SecondArgument { get; set; }
        
        public StartRenderingModel()
        {
        }

        public StartRenderingModel(StartRenderingModel startRender)
        {
            FirstArgument = startRender.FirstArgument;
            SecondArgument = startRender.SecondArgument;
        }
    }
}
