﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LogFileReaderWriter
{
    public class FileReader
    {
        public static string ReadFileSync(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentException("filePath parameter is null or empty. [FileReader].[ReadFileAsync]");
            }

            using (var memoryMappedFile = MemoryMappedFile.CreateFromFile(filePath))
            {
                using (var memoryMappedViewStream = memoryMappedFile.CreateViewStream())
                {
                    return ReadSync(memoryMappedViewStream);
                }
            }
        }

        public static async Task<string> ReadFileAsync(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentException("filePath parameter is null or empty. [FileReader].[ReadFileAsync]");
            }

            using(var memoryMappedFile = MemoryMappedFile.CreateFromFile(filePath))
            {
                using(var memoryMappedViewStream = memoryMappedFile.CreateViewStream())
                {
                    return await ReadAsync(memoryMappedViewStream);
                }
            }
        }

        private static string ReadSync(Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                var data = reader.ReadToEnd();

                return data;
            }
        }

        private static async Task<string> ReadAsync(Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                var data = await reader.ReadToEndAsync();

                return data;
            }
        }
    }
}
