﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogFileReader
{
    public class ReportFileWriter
    {
        public string FilePath { get; set; }

        public ReportFileWriter()
        {
            
        }

        public ReportFileWriter(string filePath)
        {
            FilePath = filePath;
        }

        public void WriteReportXmlFile(string report)
        {
            if (string.IsNullOrEmpty(report))
            {
                throw new ArgumentException();
            }

            using (var fileStream = new FileStream(FilePath, FileMode.Create))
            {
                using (var writer = new StreamWriter(fileStream))
                {
                    writer.WriteLine(report);
                }
            }
        }
    }
}
