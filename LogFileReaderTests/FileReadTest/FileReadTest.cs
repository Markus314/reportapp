﻿using System;
using System.Text;
using System.Collections.Generic;
using LogFileReaderWriter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using TestContext = Microsoft.VisualStudio.TestTools.UnitTesting.TestContext;

namespace LogFileReaderTests.FileReadTest
{
    /// <summary>
    /// Summary description for FileReadTest
    /// </summary>
    [TestFixture]
    public class FileReadTest
    {
        private static readonly string _filePath = @"E:\dotNET\server\server.log";
        
        [Test]
        public void ReadFileSyncWithInCorrectFilePath_Test()
        {
            // Arrange
            string filePath = "";

            // Act
            var testData = FileReader.ReadFileSync(filePath);

            // Assert
            NUnit.Framework.Assert.AreNotEqual(String.Empty, testData);
        }

        [Test]
        public void ReadFileSyncIfNotEmpty_Test()
        {
            // Arrange
            string filePath = _filePath;

            // Act
            var testData = FileReader.ReadFileSync(filePath);

            // Assert
            NUnit.Framework.Assert.AreNotEqual(String.Empty, testData);
        }
    }
}
