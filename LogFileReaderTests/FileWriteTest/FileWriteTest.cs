﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogFileReader;
using NUnit.Framework;
using NUnit.Framework.Internal;
using XMLModel;

namespace LogFileReaderTests.FileWriteTest
{
    [TestFixture()]
    public class FileWriteTest
    {
        private static readonly string _filePath = @"E:\dotNET\server\server.log";
        private ReportFileWriter _reportFileWriter;

        [SetUp]
        public void SetUp()
        {
            _reportFileWriter = new ReportFileWriter();
        }

        [Test]
        public void WriteFileTest_Test()
        {
            // Arrange
            string report = null;

            // Act

            //Assert
            Assert.Throws<ArgumentException>(() => _reportFileWriter.WriteReportXmlFile(report));
        }
    }
}
