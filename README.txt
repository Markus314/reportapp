1. This is a WPF application.
3. I haven't an experience working with Regex, so I'm not using that one for that project.
2. If you want launch it, you should look for EntryPoint project
   and watch EntryPoint class. There are placed two main properties, which stores file location.
   In correct execution, you should change file path, according to your file system model.
3. Generated Report.xml file are located inside the folder where the project solution is placed.
   Every time, you execute the project, this file is created from scratch, and all data will be 
   written to newly create .xml file.
4. Unit test code coverage is not fully, but as an example is demonstrated in the project solution
   inside 'UnitTests' folder.
5. Mostrly, the code is commented.