﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Win32;
using LogFileReaderWriter;
using Services.Converters.Interfaces;
using Services.Splitter;
using LogFileEntityModel;
using LogFileReader;
using Services.Converters.Realization;
using Services.Filter;
using Services.XmlSerialization;
using XMLModel;

namespace ReportGenerator.ViewModel
{
    /// <summary>
    /// Initializes a new instance of the ReportGeneratorVm class.
    /// </summary>
    public class ReportGeneratorVm : ViewModelBase
    {
        #region private fields

        private FrameworkElement _mainElement;

        private ICommand _generateReportXmlCommand;
        private ICommand _chooseLogFileCommand;

        // Converter declarations
        private static IRenderModelConverter<StartRenderingModel> _startRenderModelConverter;
        private static IRenderModelConverter<RenderingReturnedModel> _returnedRenderModelConverter;
        private static IRenderModelConverter<GetRenderingModel> _getRenderingModelConverter;

        private string _logFilePath;
        private string _reportFilePath;

        private bool _isLogFileOpened;

        #endregion

        #region ICommand props

        public ICommand ChooseLogFileCommannd
            => _chooseLogFileCommand ?? (_chooseLogFileCommand = new RelayCommand(ChooseLogFileCommanndMethod));

        public ICommand GenerateReportXmlCommand
            =>
                _generateReportXmlCommand ??
                (_generateReportXmlCommand = new RelayCommand(GenerateReportXmlCommandMethod));

        #endregion

        #region Properties

        public FrameworkElement MainElement
        {
            get { return _mainElement; }
            set { Set(() => MainElement, ref _mainElement, value); }
        }

        public Report Report { get; set; }

        public string LogFilePath
        {
            get { return _logFilePath; }
            set { Set(() => LogFilePath, ref _logFilePath, value); }
        }

        public string ReportFilePath
        {
            get { return _reportFilePath; }
            set { Set(() => ReportFilePath, ref _reportFilePath, value); }
        }

        public ReportGeneratorVm(FrameworkElement element)
        {
            IsLogFileOpened = false;
            MainElement = element;
        }

        public bool IsLogFileOpened
        {
            get { return _isLogFileOpened; }
            set { Set(() => IsLogFileOpened, ref _isLogFileOpened, value); }
        }

        #endregion

        public async Task OnLoad()
        {
            Initialize();
        }

        #region Command methods

        public async void ChooseLogFileCommanndMethod()
        {
            var fileDialog = new OpenFileDialog
            {
                DefaultExt = ".txt"
            };

            var isOpened = fileDialog.ShowDialog();

            if (isOpened.Value)
            {
                LogFilePath = fileDialog.FileName;

                IsLogFileOpened = isOpened.Value;
                
                Report = await ConvertLogDataToReportModel(LogFilePath);
            }
        }

        public async void GenerateReportXmlCommandMethod()
        {
            var saveFileDialog = new SaveFileDialog();

            saveFileDialog.FileName = "Report";
            saveFileDialog.DefaultExt = ".xml";

            var isSaved = saveFileDialog.ShowDialog();

            if (isSaved.Value)
            {
                IsLogFileOpened = false;

                ReportFilePath = saveFileDialog.FileName;
                await WriteXmlReport(ReportFilePath);
            }
        }

        #endregion

        #region Private methods
        
        private static void Initialize()
        {
            _startRenderModelConverter = new ToStartRenderConverter();
            _returnedRenderModelConverter = new ToRenderReturnedConverter();
            _getRenderingModelConverter = new ToGetRenderConverter();
        }

        private async Task<Report> ConvertLogDataToReportModel(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentException();
            }

            var logData = await ReadFile(filePath);
            var splittedLogData = SplitData(logData);
            
            var report = LaunchConverters(splittedLogData);
            
            return report;
        }

        private Report LaunchConverters(IList<string> splittedLogData)
        {
            // Create main parser for particalar rendering log 
            var logStringParser =
                new LogStringParser(
                    _startRenderModelConverter,
                    _returnedRenderModelConverter,
                    _getRenderingModelConverter,
                    splittedLogData);

            // Launch converters for particalar log
            var startRenderList = logStringParser.ConvertToStartRenderList();
            var startRenderReturnedList = logStringParser.ConvertToRenderReturnedList();
            var getRenderingList = logStringParser.ConvertToGetRenderingList();

            // Filter data for report creating 
            var filter = new FilterRenderData(startRenderList, startRenderReturnedList, getRenderingList);
            var report = filter.FilterStartRendering();

            return report;
        }

        private IList<string> SplitData(string logData)
        {
            var splitter = new Splitter();
            return splitter.Split(logData);
        }

        private async Task<string> ReadFile(string filePath)
        {
            return await FileReader.ReadFileAsync(filePath);
        }

        private async Task WriteXmlReport(string reportFilePath)
        {
            if (string.IsNullOrWhiteSpace(reportFilePath))
            {
                throw  new ArgumentException();
            }

            // Serialize report into xml format and return as string
            var serialization = new XmlSerialization();
            var serializedReport = serialization.SerializeIntoXml(Report);


            // Write report data into Report.xml file
            var reportFileWriter = new ReportFileWriter(ReportFilePath);
            reportFileWriter.WriteReportXmlFile(serializedReport);
        }

        #endregion
    }
}