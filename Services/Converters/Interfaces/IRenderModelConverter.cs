﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Converters.Interfaces
{
    public interface IRenderModelConverter<T>
        where T : class, new()
    {
        IList<T> Convert(IList<string> dataList);
    }
}
