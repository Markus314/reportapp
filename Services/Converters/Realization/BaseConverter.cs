﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogFileEntityModel.PropertiesModels;

namespace Services.Converters.Realization
{
    /// <summary>
    /// Converter that contains common methods 
    /// for all available converters
    /// </summary>
    public class BaseConverter
    {
        public DateModel DateModelConverter(string date)
        {
            if(string.IsNullOrWhiteSpace(date))
                throw new ArgumentException();

            return DateModelConverterPrivate(date);
        }

        public UidModel GetUid(string uidParam)
        {
            if (string.IsNullOrWhiteSpace(uidParam))
                throw new ArgumentException();

            return GetUID(uidParam);
        }


        /// <summary>
        /// input: uid as string type
        /// </summary>
        /// <param name="uidParam"></param>
        /// <returns>UidModel with right and left properties and overriden ToString() method</returns>
        private UidModel GetUID(string uidParam)
        {
            if (string.IsNullOrEmpty(uidParam))
            {
                throw new ArgumentException();
            }

            var splittedIds = uidParam.Split('-');

            var uid = new UidModel();
            uid.IdLeftPart = long.Parse(splittedIds[0]);
            uid.IdRightPart = long.Parse(splittedIds[1]);

            return uid;
        }

        /// <summary>
        /// input: date as string format
        /// YYYY-MM-DD HH:mm:ss,SSS
        /// </summary>
        /// <param name="date"></param>
        /// <returns>DateModel object</returns>
        private DateModel DateModelConverterPrivate(string date)
        {
            if (string.IsNullOrEmpty(date))
            {
                throw new ArgumentException("<date> parameter is null or empty. [StringToGetRenderingCommandConverter].[DateTimeConverter(...)]");
            }

            var splittedDateStrs = date.Split(',');
            var dateModel = new DateModel();
            dateModel.Date = DateTime.Parse(splittedDateStrs[0]);
            dateModel.SSS = splittedDateStrs[1];

            return dateModel;
        }
    }
}
