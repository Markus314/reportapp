﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogFileEntityModel;
using Services.Converters.Interfaces;

namespace Services.Converters.Realization
{
    public class LogStringParser
    {
        private string _startRenderOptions = "Executing request startRendering";
        private string _startRenderReturned = "Service startRendering returned";
        private string _getRenderingString = "Executing request getRendering";
        private string _logMode = "INFO";

        public IRenderModelConverter<StartRenderingModel> StartRenderModelConverter { get; set; }
        public IRenderModelConverter<RenderingReturnedModel> StartRenderReturnedModelConverter { get; set; }
        public IRenderModelConverter<GetRenderingModel> GetRenderingModelConverter { get; set; }
        
        public IList<string> LogDataList { get; private set; }

        public LogStringParser()
        {
            
        }

        public LogStringParser(
            IRenderModelConverter<StartRenderingModel> starToRenderModelConverter, 
            IRenderModelConverter<RenderingReturnedModel> startRenderReturnedModelConverter, 
            IRenderModelConverter<GetRenderingModel> getRenderingModelConverter, 
            IList<string> logDataList)
        {
            StartRenderModelConverter = starToRenderModelConverter;
            StartRenderReturnedModelConverter = startRenderReturnedModelConverter;
            GetRenderingModelConverter = getRenderingModelConverter;

            LogDataList = GetListThatContainsParameterString(logDataList, _logMode);
        }

        public IList<StartRenderingModel> ConvertToStartRenderList()
        {
            var splittedByLogInfoModel = GetListThatContainsParameterString(LogDataList, _startRenderOptions);

            var startRenderModelLis = StartRenderModelConverter.Convert(splittedByLogInfoModel);

            return startRenderModelLis;
        }

        public IList<RenderingReturnedModel> ConvertToRenderReturnedList()
        {
            var splittedByInfoLogModel = GetListThatContainsParameterString(LogDataList, _startRenderReturned);

            var startRenderReturnModelList = StartRenderReturnedModelConverter.Convert(splittedByInfoLogModel);

            return startRenderReturnModelList;
        }

        public IList<GetRenderingModel> ConvertToGetRenderingList()
        {
            var splittedByInfoModel = GetListThatContainsParameterString(LogDataList, _getRenderingString);

            var getRenderingModelList = GetRenderingModelConverter.Convert(splittedByInfoModel);

            return getRenderingModelList;
        }

        private IList<string> GetListThatContainsParameterString(IList<string> container, string parameter)
        {
            if (container != null && !string.IsNullOrWhiteSpace(parameter))
            {
                return container.Where(data => data.Contains(parameter)).ToList();
            }

            throw new ArgumentException();
        }
    }
}
