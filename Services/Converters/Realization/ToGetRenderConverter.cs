﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LogFileEntityModel;
using LogFileEntityModel.Enum;
using LogFileEntityModel.PropertiesModels;
using Services.Converters.Interfaces;

namespace Services.Converters.Realization
{
    /// <summary>
    /// This converter just takes dataList of 
    /// log data and trying to convert that info
    /// into Log model for 'getRendering' command
    /// </summary>
    /// <param name="dataList"></param>
    public class ToGetRenderConverter : BaseConverter, IRenderModelConverter<GetRenderingModel>
    {
        /// <summary>
        /// Convert string data into
        /// List of 'getRendering' objects
        /// </summary>
        /// <param name="dataList"></param>
        /// <returns> IList<LogFileGetRenderingCommandModel> </returns>
        public IList<GetRenderingModel> Convert(IList<string> dataList)
        {
            var splitCharList = new char[] { '[', ']', ';' };
            var logFileStartRenderList = new List<GetRenderingModel>();

            foreach (var data in dataList)
            {
                var splittedString = data.Split(splitCharList, StringSplitOptions.RemoveEmptyEntries)
                                         .ToList();

                var getRendering = new GetRenderingModel
                {
                    Date = DateModelConverter(splittedString[0]),
                    Process = splittedString[1],
                    LogMode = LogMode.Info,
                    Provider = splittedString[3],
                    Uid = GetUid(splittedString[5]),
                    LogMessage = data
                };
                logFileStartRenderList.Add(getRendering);
            }

            return logFileStartRenderList;
        }
    }
}
