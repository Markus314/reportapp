﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogFileEntityModel;
using LogFileEntityModel.Enum;
using LogFileEntityModel.PropertiesModels;
using Services.Converters.Interfaces;

namespace Services.Converters.Realization
{
    /// <summary>
    /// This converter just takes dataList of 
    /// log data and trying to convert that info 
    /// into Log model for 'startRendering returned' command
    /// </summary>
    /// <param name="dataList"></param>
    public class ToRenderReturnedConverter : BaseConverter, IRenderModelConverter<RenderingReturnedModel>
    {
        /// <summary>
        /// Convert string data into
        /// List of 'startRenderReturned' objects
        /// </summary>
        /// <param name="dataList"></param>
        /// <returns>IList<LogFileStartRenderingReturnedCommandModel> </returns>
        public IList<RenderingReturnedModel> Convert(IList<string> dataList)
        {
            // Splitter for StartRender Returned log file model
            var splittCharList = new char[] { '[', ']', ';'};

            var logFileStartRenderList = new List<RenderingReturnedModel>();

            foreach (var data in dataList)
            {
                var splittedString = data.Split(splittCharList, StringSplitOptions.RemoveEmptyEntries)
                                         .ToList();

                var uidString = splittedString[4].Split(' ');

                var renderingReturendModel = new RenderingReturnedModel
                {
                    Date = DateModelConverter(splittedString[0]),
                    Process = splittedString[1],
                    LogMode = LogMode.Info,
                    Provider = splittedString[3],
                    Uid = GetUid(uidString[4]),
                    LogMessage = data
                };

                logFileStartRenderList.Add(renderingReturendModel);
            }

            return logFileStartRenderList;
        }
    }
}
