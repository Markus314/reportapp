﻿using LogFileEntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogFileEntityModel.Enum;
using LogFileEntityModel.PropertiesModels;
using Services.Converters.Interfaces;

namespace Services.Converters.Realization
{
    /// <summary>
    /// This converter just takes dataList of 
    /// log data and trying to convert that info 
    /// into Log model for 'startRendering' command
    /// </summary>
    /// <param name="dataList"></param>
    public class ToStartRenderConverter : BaseConverter, IRenderModelConverter<StartRenderingModel>
    {

        /// <summary>
        /// Convert string data into
        /// List of 'startRendering' objects
        /// </summary>
        /// <param name="dataList"></param>
        /// <returns> IList<LogFileStartRenderingCommandModel> </returns>
        public IList<StartRenderingModel> Convert(IList<string> dataList)
        {
            var splittCharList = new char[] {'[', ']', ';'};
            var startRenderingList = new List<StartRenderingModel>();
            
            foreach (var data in dataList)
            {
                var splittedString = data.Split(splittCharList, StringSplitOptions.RemoveEmptyEntries).ToList();

                var startRenderingModel = new StartRenderingModel();

                startRenderingModel.Date = DateModelConverter(splittedString[0]);
                startRenderingModel.Process = splittedString[1];
                startRenderingModel.LogMode = LogMode.Info;
                startRenderingModel.Provider = splittedString[3];

                var parameters = StartRenderParametersConverter(splittedString[5]);

                startRenderingModel.FirstArgument = parameters[0];
                startRenderingModel.SecondArgument = parameters[1];
                startRenderingModel.Message = data;

                startRenderingList.Add(startRenderingModel);
            }

            return startRenderingList;
        }

        /// <summary>
        /// Converts parameters for startRendering object
        /// takes string 
        /// </summary>
        /// <param name="renderParameters"></param>
        /// <returns> List with two elements of int paramaters </returns>
        private IList<int> StartRenderParametersConverter(string renderParameters)
        {
            if (string.IsNullOrEmpty(renderParameters))
            {
                throw new ArgumentException("<renderParameters> parameter is null or empty. [StringToStartRenderRequestConverter].[ToStartRenderParametersConverter(...)]");
            }

            var parameters = new List<int>();

            var splittedParametersStrs = renderParameters.Split(',').ToList();
            
            splittedParametersStrs.ForEach(param =>
            {
                parameters.Add(int.Parse(param));
            });

            return parameters;
        }
    }
}
