﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using LogFileEntityModel;
using LogFileEntityModel.PropertiesModels;
using Services.Filter.Interfaces;
using XMLModel;

namespace Services.Filter
{
    public class FilterRenderData : IFilterRenderData
    {
        #region Properties

        public IList<StartRenderingModel> StartRenderingList { get; private set; }

        public IList<RenderingReturnedModel> RenderingReturnedList { get;
            private set; }

        public IList<GetRenderingModel> GetRenderingList { get; private set; }
        public IList<int> DocumentIds { get; set; }

        #endregion

        #region Constructors

        public FilterRenderData()
        {
        }

        public FilterRenderData(IList<StartRenderingModel> logFileStartRenderOptionsModels,
            IList<RenderingReturnedModel> logFileStartRenderReturnedOptionsModels,
            IList<GetRenderingModel> logFileGetRenderingCommandModels)
        {
            StartRenderingList = logFileStartRenderOptionsModels;
            RenderingReturnedList = logFileStartRenderReturnedOptionsModels;
            GetRenderingList = logFileGetRenderingCommandModels;

            DocumentIds = GetDocumentIdsDistinct(StartRenderingList);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Filter all list of log file objects
        /// </summary>
        /// <returns>Report object</returns>
        public Report FilterStartRendering()
        {
            var report = new Report();

            var renderings = GenerateReportRenderings();
            var summary = GenerateRenderingSumary(renderings);

            report.Rendering = renderings.ToList();
            report.Summary = summary;

            return report;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Geterate rendering summary for 
        /// all rendering that were processed
        /// while executing
        /// </summary>
        /// <param name="renderings"></param>
        /// <returns>Summary object</returns>
        private Summary GenerateRenderingSumary(IList<Rendering> renderings)
        {
            var summary = new Summary();

            summary.Count = renderings.Count;

            foreach (var rendering in renderings)
            {
                var renderCount = rendering.Start.Count;
                var duplicates = rendering.Get.Count;

                summary.Duplicates += renderCount > 1 ? 1 : 0;
                summary.Unnecessary += duplicates > 0 ? 0 : 1;
            }

            return summary;
        }

        /// <summary>
        /// Generates list of rendering objects
        /// </summary>
        /// <returns>
        /// 'IList<Rendering>'
        /// </returns>
        private IList<Rendering> GenerateReportRenderings()
        {
            var renderings = new List<Rendering>();

            foreach (var documentId in DocumentIds)
            {
                renderings.Add(Generaterendering(documentId));
            }

            return renderings;
        }

        private Rendering Generaterendering(int documentId)
        {
            var rendering = new Rendering();

            // Get startrendering for the document
            var startRenderingDocIdList =
                StartRenderingList.Where(rend => rend.FirstArgument == documentId)
                                                      .ToList();

            var renderReturned = new List<RenderingReturnedModel>();

            startRenderingDocIdList.ForEach(startRender =>
            {
                renderReturned.Add(
                    RenderingReturnedList.FirstOrDefault(
                        returned => returned.Process == startRender.Process));
            });

            var getRenderings = new List<GetRenderingModel>();

            renderReturned.ForEach(returned =>
            {
                getRenderings.Add(GetRenderingList.FirstOrDefault(getRend => getRend.Uid.Equals(returned.Uid)));
            });

            rendering = InitializedRendering(documentId, startRenderingDocIdList, getRenderings);

            return rendering;
        }

        /// <summary>
        /// Initialize Rendering object
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="startRender"></param>
        /// <param name="getRendering"></param>
        /// <param name="uidForStartRender"></param>
        /// <returns>Rendering object</returns>
        private Rendering InitializedRendering(
            int documentId,
            List<StartRenderingModel> startRender,
            List<GetRenderingModel> getRendering)
        {
            var rendering = new Rendering
            {
                Document = documentId.ToString(),
                Page = startRender.FirstOrDefault().SecondArgument ?? 0,
                Uid = getRendering.FirstOrDefault().Uid
            };

            startRender.ForEach(start => { rendering.Start.Add(start.Date); });

            getRendering.ForEach(getRendr => { rendering.Get.Add(getRendr.Date); });

            return rendering;
        }

        /// <summary>
        /// For getting documents id 
        /// But onlu distinct
        /// </summary>
        /// <param name="logFileStartRenderOptionsModels"></param>
        /// <returns></returns>
        private IList<int> GetDocumentIdsDistinct(
            IList<StartRenderingModel> logFileStartRenderOptionsModels)
        {
            if (logFileStartRenderOptionsModels == null)
            {
                throw new ArgumentException();
            }

            var documentIds = logFileStartRenderOptionsModels.Select(id => id.FirstArgument ?? 0).Distinct().ToList();

            return documentIds;
        }

        #endregion
    }
}