﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogFileEntityModel;
using XMLModel;

namespace Services.Filter.Interfaces
{
    public interface IFilterRenderData
    {
        IList<StartRenderingModel> StartRenderingList { get; }
        IList<RenderingReturnedModel> RenderingReturnedList { get;  }
        IList<GetRenderingModel> GetRenderingList { get;  }
        IList<int> DocumentIds { get; set; }

        Report FilterStartRendering();
    }
}
