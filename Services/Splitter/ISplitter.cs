﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Splitter
{
    public interface ISplitter
    {
        IList<string> Split(string stringToSplit);
    }
}
