﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Splitter
{
    public class Splitter : ISplitter
    {
        //private static string _regexSplitPattern = @"\s([0-9]{4})-([0-9]{2})-([0-9]{2})\s([0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3})\s";

        public IList<string> Split(string stringToSplit)
        {
            if (string.IsNullOrWhiteSpace(stringToSplit))
            {
                throw new ArgumentException("stringToSplit parameter is null or emplty. [Splitter].[Split(...)]");
            }

            var splittedStrList = stringToSplit.Split('\n');

            return splittedStrList;
        }
    }
}
