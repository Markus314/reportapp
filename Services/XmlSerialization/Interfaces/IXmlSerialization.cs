﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLModel;

namespace Services.XmlSerialization.Interfaces
{
    public interface IXmlSerialization
    {
        string SerializeIntoXml(Report report);
    }
}
