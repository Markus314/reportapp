﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Services.XmlSerialization.Interfaces;
using XMLModel;

namespace Services.XmlSerialization
{
    public class XmlSerialization : IXmlSerialization
    {
        /// <summary>
        /// Takes report object 
        /// and serialize it 
        /// into XML format
        /// </summary>
        /// <param name="report"></param>
        /// <returns>serialized object into xml converted to string</returns>
        public string SerializeIntoXml(Report report)
        {
            if (report == null)
            {
                throw new ArgumentException();
            }

            var stringWriter = new StringWriter();

            var xmlsNamespace = new XmlSerializerNamespaces();
            xmlsNamespace.Add(string.Empty, string.Empty);

            var serializer = new XmlSerializer(report.GetType());
            
            serializer.Serialize(stringWriter, report, xmlsNamespace);

            return stringWriter.ToString();
        }

        public Report DeserializeToObject(string xmlReport)
        {
            if (string.IsNullOrEmpty(xmlReport))
            {
                throw new ArgumentException();
            }
            
            var stringReader = new StringReader(xmlReport);
            var xmlSerializer = new XmlSerializer(typeof(Report));
            var report = xmlSerializer.Deserialize(stringReader) as Report;

            return report;
        }
    }
}
