﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using LogFileEntityModel.PropertiesModels;
using NUnit.Framework;
using Services.Converters.Realization;

namespace ServicesTests.ConvertersTest
{
    public class BaseConverterTests
    {
        private BaseConverter _baseConverter;

        [SetUp]
        public void SetUp()
        {
            _baseConverter = new BaseConverter();
        }

        [Test]
        public void DateModelConverterWhenArgumentNull_Test()
        {
            // Arrange
            string dateStr = string.Empty;

            // Act
            
            // Assert
            Assert.Throws<ArgumentException>(() => _baseConverter.DateModelConverter(dateStr));
        }

        [Test]
        public void UidModelConverterWhenArgumentNull_Test()
        {
            // Arrange
            string uidStr = string.Empty;

            // Act
            
            // Assert
            Assert.Throws<ArgumentException>(() => _baseConverter.GetUid(uidStr));
        }
    }
}
