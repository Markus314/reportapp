﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Services.Splitter;

namespace ServicesTests.SplitterTests
{
    [TestFixture]
    public class SplitterTest
    {
        private ISplitter _splitter;

        [SetUp]
        public void SetUp()
        {
            _splitter = new Splitter();
        }

        [Test]
        public void SplitWhenParamIsNull_Test()
        {
            // Arrange
            string stringToSplit = null;

            // Act
            
            // Assert
            Assert.Throws<ArgumentException>(() => _splitter.Split(stringToSplit));
        }

        [Test]
        public void SplitWhenParamIsWhiteSpace_Test()
        {
            // Arrange
            var stringToSplit = string.Empty;

            // Act

            // Assert
            Assert.Throws<ArgumentException>(() => _splitter.Split(stringToSplit));
        }

        [Test]
        public void SplitWhenParamIsNotNull_Test()
        {
            // Arrange
            var stringToSplit = "word1 word2 word3, word4....";

            // Act
            var returnValue = _splitter.Split(stringToSplit);

            // Assert
            Assert.AreNotSame(stringToSplit, returnValue);
        }
    }
}
