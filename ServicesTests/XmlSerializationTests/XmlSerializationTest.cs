﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Internal;
using NUnit.Framework;
using Services.XmlSerialization;
using Services.XmlSerialization.Interfaces;
using XMLModel;
using LogFileEntityModel.PropertiesModels;

namespace ServicesTests.XmlSerializationTests
{
    [TestFixture]
    public class XmlSerializationTest
    {
        private IXmlSerialization _xmlSerialization;

        [SetUp]
        public void SetUp()
        {
            _xmlSerialization = new XmlSerialization();
        }

        [Test]
        public void XmlSerializationWhenArgumentNull_Test()
        {
            // Arrange
            Report report = null;

            // Act

            // Assert
            Assert.Throws<ArgumentException>(() => _xmlSerialization.SerializeIntoXml(report));
        }

        [Test]
        public void XmlSerializationWhenReturnValueNotNull_Test()
        {
            // Arrange
            var report = GenerateReportMockObject();

            // Act
            var returnValue = _xmlSerialization.SerializeIntoXml(report);

            // Assert
            Assert.NotNull(returnValue);
        }

        #region Private methods

        private Report GenerateReportMockObject()
        {
            var report = new Report();
            
            report.Rendering.Add(GenerateRenderingMockObject());
            report.Summary = GenerateSummaryMockObject(report);

            return report;
        }

        private Rendering GenerateRenderingMockObject()
        {
            var rendering = new Rendering
            {
                Uid = new UidModel(),
                Document = "doc",
                Page = 0
            };

            var dateModel = new DateModel
            {
                Date = DateTime.Now,
                SSS = "123"
            };

            rendering.Start.Add(dateModel);
            rendering.Get.Add(dateModel);

            return rendering;
        }

        private Summary GenerateSummaryMockObject(Report report)
        {
            var summary = new Summary
            {
                Count = report.Rendering.Count,
                Duplicates = 0,
                Unnecessary = 0
            };

            return summary;
        }

        #endregion
    }
}
