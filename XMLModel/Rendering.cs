﻿using LogFileEntityModel.PropertiesModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XMLModel
{
    [Serializable]
    [XmlType(AnonymousType = true)]
    public class Rendering
    {
        public Rendering()
        {
            Start = new List<DateModel>();
            Get = new List<DateModel>();
        }

        public Rendering(string document, int page, UidModel uid)
            : this()
        {
            Document = document;
            Page = page;
            Uid = uid;
        }

        [XmlElement("document")]
        public string Document
        {
            get; set;
        }

        [XmlElement("page")]
        public int Page
        {
            get; set;
        }

        [XmlElement("uid")]
        public UidModel Uid
        {
            get; set;
        }

        [XmlElement("start")]
        public List<DateModel> Start
        {
            get; set;
        }

        [XmlElement("get")]
        public List<DateModel> Get
        {
            get; set;
        }
    }
}
