﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XMLModel
{
    [Serializable]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false,ElementName = "report")]
    public class Report
    {
        public Report()
        {
            Rendering = new List<Rendering>();
        }
        
        [XmlElement("rendering")]
        public List<Rendering> Rendering
        {
            get; set;
        }

        [XmlElement("summary")]
        public Summary Summary
        {
            get; set;
        }
    }
}
