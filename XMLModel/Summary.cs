﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XMLModel
{
    [Serializable]
    [XmlType(AnonymousType = true)]
    public class Summary
    {
        [XmlElement("count")]
        public int Count
        {
            get; set;
        }

        [XmlElement("duplicates")]
        public int Duplicates
        {
            get; set;
        }

        [XmlElement("unnecessary")]
        public int Unnecessary
        {
            get; set;
        }
    }
}
